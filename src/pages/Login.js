import UserContext from '../UserContext';
import { Redirect } from 'react-router-dom';
import { Form, Button, Container } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import Hero from '../components/Banner';
import Footer from '../pages/Footer';
import Swal from 'sweetalert2';


const bannerLabel = {
    title: 'User Login',
    tagline: 'Access your account and start check our best products here!'
}

export default function Login() {
    const {user, setUser} = useContext(UserContext)

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

        const authenticate = (e) => {
            e.preventDefault()

            fetch('https://stark-ravine-07314.herokuapp.com/users/login',{
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })
            .then(res => res.json())
            .then(data => {
                if (typeof data.accessToken !== "undefined") {
                    localStorage.setItem('accessToken', data.accessToken);
                    retrieveUserDetails(data.accessToken);
                    Swal.fire({
                        title: "Login Successful",
                        icon: "success",
                        text: "Welcome to Unik's E-Commerce App !"
                    });
                } else {
                    Swal.fire({
                        title: "Login Failed",
                        icon: "danger",
                        text: "Check your login details and try again!"
                    });
                }
            })
        }

        const retrieveUserDetails = (token) => {
            fetch('https://stark-ravine-07314.herokuapp.com/users/details', {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(result => result.json())
            .then(convertedResult => {
                console.log(convertedResult);

                setUser({
                    id: convertedResult._id,
                    isAdmin: convertedResult.isAdmin
                });
                
                        console.log(convertedResult);
            })
        }

        useEffect(()=>{
            if (email !== "" && password !== "") {
                setIsActive(true)
            } else {
                setIsActive(false)
            }
        }, [email, password]);
    return(
        (user.id &&  user.isAdmin === true) ? 
        <Redirect to="/products/all"/>:  (user.id &&  user.isAdmin === false) ? <Redirect to="/products"/> :
        <card>
            <Container>
                {/* Banner of the page */}
                <Hero data={bannerLabel}/>

                <Form className="mb-5" onSubmit={e => authenticate(e)}>
                    <Form.Group controlId="userEmail">
                        <Form.Label> Email Address: </Form.Label>
                        <Form.Control 
                        type="email" 
                        placeholder="Insert your Email Here"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required/>
                    </Form.Group>
                    <Form.Group controlId="userPassword">
                        <Form.Label> Password: </Form.Label>
                        <Form.Control 
                        type="password" 
                        placeholder="Insert your Password Here"
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        required/>
                    </Form.Group>
                    {
                        isActive ?
                        <Button type="submit" variant="success" id="submitBtn" className="btn btn-block"> Click Here </Button>
                        :
                        <Button type="submit" variant="warning" id="submitBtn" className="btn btn-block" disabled> Click Here </Button>
                    }
                </Form>
            </Container>
            <Footer foot="fixed-bottom"/>
        </card>
    );
};