import { Row, Col} from "react-bootstrap";

const Footer = (props) => (
    <div className={"border border-top bg-light text-dark " + props.foot}>
        {/* <Row className="justify-content-center text-center">
            <Col className="d-flex justify-content-start pl-5">
                <h5>
                    Booking App
                </h5>
            </Col>
            <Col className="d-flex justify-content-end pr-5">
                <h5>
                    Zuitt Coding Bootcamp
                </h5>                
            </Col>
        </Row> */}
        <Row className="pt-3 justify-content-center text-center">
            <Col>
                <h5>
                    2021 &copy; UNik
                </h5>
            </Col>
        </Row>
    </div>
);

export default Footer;