import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { Link , useParams } from 'react-router-dom';
import UserContext from '../UserContext';
import { useState, useEffect, useContext } from 'react';

export default function ProductView() {
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const { productId } = useParams();

	const { user } = useContext(UserContext);

	useEffect(()=> {

		console.warn(productId);

		fetch(`https://stark-ravine-07314.herokuapp.com/products/selected`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json" 
			},
			body: JSON.stringify({
				id: productId
			})
		})
		.then(res => { 
            console.log(res)
            if (res.ok) {
                return res.json()
            } else {
                throw new Error("Error Occured")
            }
        }).then(data => {
            console.log(data)
            setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

        }).catch(error => console.error(error))

	}, [productId]);

    return (
        <Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>  </Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text> {description} </Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>&#8369; {price} </Card.Text>
						
							{ (user.id) ? 
								<Button variant="success" block onClick={() => enroll(productId)}>Add to Cart</Button>
									: 
								<Link className="btn btn-warning btn-block" to="/login">Log in to Buy</Link>
	 						}
						
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
    )
 }