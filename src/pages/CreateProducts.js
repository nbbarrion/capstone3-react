
import { useContext, useState } from 'react';
import { Form, Button } from "react-bootstrap";
import { Redirect, useHistory } from 'react-router';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function CreateProduct() {
   
    const history = useHistory();
    const {user} = useContext(UserContext);
    const [ name, setName ]= useState('');
    const [ description, setDescription ] = useState('');
    const [ price, setPrice ] = useState('');

    console.log(user);
    function toCreate(event) {
        event.preventDefault();

        fetch('https://stark-ravine-07314.herokuapp.com/products/add', {
            method: "POST", 
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("accessToken")}`,
                'Content-Type': 'application/json' 
            },
            body: JSON.stringify({ 
                name: name,
                description: description,
                price: price
            })
        }) 
        .then(res => { 
            console.log(res)
            if (res.ok) {
                return res.json()
            } else {
                throw new Error("Error Occured")
            }
        }).then(data => {
            console.log(data)
            Swal.fire({
                title:`${name} was Successfully Created!`,
                icon: 'success'
            })


        }).catch(error => console.error(error))
    }   

    const reRoute = () => {
        if(user.isAdmin === false){
   
           Swal.fire({
               title:`Please Login as an Admin`,
               icon: 'error'
           })
           
           localStorage.removeItem('accessToken')
   
            history.push("/login")
            
            location.reload()
        }
    }
   

    return(
            (user.isAdmin === false) ? reRoute()
             
            :
            
        
        <>
           <Form className="m-5" onSubmit={event => toCreate(event)}>
               <Form.Group>
                   <Form.Label>
                       Name:
                   </Form.Label>
                   <Form.Control type="text" placeholder="Please Insert Name" value={name} onChange={event => setName (event.target.value)} required/>
               </Form.Group>
               <Form.Group>
                   <Form.Label>
                       Description:
                   </Form.Label>
                   <Form.Control type="text" placeholder="Please Insert Description Here" value={description} onChange={event => setDescription (event.target.value)} required/>
               </Form.Group>
               <Form.Group>
                   <Form.Label>
                       Price:
                   </Form.Label>
                   <Form.Control type="number" placeholder="Please Insert Price Here" value={price} onChange={event => setPrice (event.target.value)} required/>
               </Form.Group>
               <Button variant="success" className="btn btn-block" type="submit" id="submitBtn">
                    Create New Product
                </Button>
           </Form>
        </>
    );
}