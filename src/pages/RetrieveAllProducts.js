import { React, useState, useEffect, useContext } from 'react';
import { Card, Container } from 'react-bootstrap';
import UserContext from '../UserContext';

import { useHistory } from 'react-router';
import Swal from 'sweetalert2';

export default function RetrieveAllProducts() {

    const history = useHistory();
    const {user} = useContext(UserContext);
    const [ products, setProducts ] = useState([]);
    const [ checker, setChecker ] = useState(false);
    useEffect(() => {
        const data = {
            method: "GET",
            headers: {
                "Authorization" : `Bearer ${localStorage.getItem("accessToken")}`,
                "Content-Type" : "application/json"
            }
        }
        fetch('https://stark-ravine-07314.herokuapp.com/products/all', data)
        .then(result => {
            if (result.ok) {
                return result.json()
            } else {
                throw new Error("Error Occured")
            }
        })
        .then(data => {
            setProducts(data.map(product => {
                console.log(product)
                return (
                    <>
                        <Card>
                            <Card.Header>
                                <Card.Title>
                                    {product.name}
                                </Card.Title>
                            </Card.Header>
                            <Card.Body>
                                <Card.Text>
                                    {product.description}
                                </Card.Text>
                            </Card.Body>
                            <Card.Footer>
                                <Card.Subtitle>
                                    {product.isActive.toString()}
                                </Card.Subtitle>
                            </Card.Footer>
                        </Card>
                    </>
                )
            }))
        }).catch(error => console.error(error))
        if (checker) {
            setChecker(false)
        } else {
            setChecker(true)
        }
    }, [])

 const reRoute = () => {
     if(user.isAdmin === false){

        Swal.fire({
            title:`please login as an admin`,
            icon: 'error'
        })
        
        localStorage.removeItem('accessToken')

         history.push("/login")
         
         location.reload()
     }
 }

    return (
        (user.isAdmin === false) ?  reRoute()
             
        :
        
        <>
            <Container>
                {checker && products}
            </Container>
        </>
    )
}

