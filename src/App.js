import { useState, useEffect } from 'react';
import './App.css';

import Home from './pages/Home';
import Footer from './pages/Footer';
import Products from './pages/Catalog';
import ProductView from './pages/ProductView';
import Login from './pages/Login';
import Signup from './pages/Register';
import Logout from './pages/Logout';
import Error from './pages/Error';
import AppNavbar from './components/Navbar';


import { Route, Switch, BrowserRouter as Router} from 'react-router-dom';

import { UserProvider } from './UserContext';
import RetrieveAllProducts from './pages/RetrieveAllProducts';
import CreateProduct from './pages/CreateProducts';
  
function App() {
  const [ user, setUser ] = useState ({
    id: null,
    isAdmin: null
  });

    const unsetUser = () => {
      localStorage.clear(); 
      setUser({
        id: null,
        isAdmin: null
      })
    }

    useEffect(() => {
      //lets create a checker to identify the current status of the user
      console.log(user);
      //lets create a fetch request
      fetch('https://stark-ravine-07314.herokuapp.com/users/details', {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                }
            })
            .then(result => result.json())
            .then(convertedResult => {
              if (convertedResult._id !== "undefined") {
                setUser({
                  id: convertedResult._id,
                  isAdmin: convertedResult.isAdmin
                })
              } else {
                setUser({
                  id: null,
                  isAdmin: null
                })
              }
          })
    },[]);
    
  return (
      <Router>
         <UserProvider value={{user, unsetUser, setUser}}>
          <AppNavbar />

            <Switch>
              <Route exact path="/" component = {Home} />
              <Route exact path="/products" component = {Products} />
              <Route exact path="/login" component = {Login}/>
              <Route exact path="/register" component = {Signup} />
              <Route exact path="/logout" component = {Logout} />
              <Route exact path="/products/all" component = {RetrieveAllProducts} />
              <Route exact path="/products/create" component = {CreateProduct} />
              <Route exact path="/products/:productId" component = {ProductView} />
              <Route component = {Error} />
              <Footer/>
            </Switch>
        </UserProvider>
      </Router>
  );
}

export default App;
