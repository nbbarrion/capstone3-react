import { useContext } from 'react';
import { Navbar, Nav} from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import Logo from "../assets/logo.png";
import "./Navbar.css"
import UserContext from '../UserContext';

export default function AppNavbar() {
  const { user } = useContext(UserContext)
  // const [openLinks, setOpenLinks] = useState(false);

  // const toggleNavbar = () => {
  //   setOpenLinks(!openLinks);
  // };
    return (
      <Navbar bg="dark" expand="xl">
        <img src={Logo}
        width="120"
        height="60"
        className="d-inline-block align-top"
        alt="Image Unavailable" 
        />
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse className="justify-content-end" id="basic-navbar-nav">
                <Nav.Link className="text-warning" as={NavLink} to="/" >Home</Nav.Link>
                <Nav.Link className="text-warning" as={NavLink} to="/products" >Products</Nav.Link>
                {(user.id) ?
                <>
                  <Nav.Link className="text-warning" as={NavLink} to="/products/create" >Create a New Product</Nav.Link>
                  <Nav.Link className="text-warning" as={NavLink} to="/products/all" >Admin All Products</Nav.Link>
                  <Nav.Link className="text-warning" as={NavLink} to="/logout" >Logout</Nav.Link>
                </>
                :
                  <>
                    <Nav.Link className="text-warning" as={NavLink} to="/register" >Register</Nav.Link>
                    <Nav.Link className="text-warning" as={NavLink} to="/login" >Login</Nav.Link>
                  </>
                }
            </Navbar.Collapse>
      </Navbar>
    )
};