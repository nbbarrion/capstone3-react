import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


export default function Banner({data}) {
    const { tagline } = data;

    return (
        <Row>
            <Col className="p-5 text-center">
                
                <h3> {tagline} </h3>
            </Col>
        </Row>
    )
}